<?php

class M_dashboard extends CI_Model
{
    public function get_career()
    {
        // some rules
        $data = array('is_deleted' => NULL,);
        $query = $this->db->get_where('career', $data);

        return $query->result();
    }

    public function create_career($data)
    {
        return ($this->db->insert('career', $data))  ?   $this->db->insert_id()  :   false;
    }

    public function delete_career($id)
    {
        // soft delete
        $data = array('is_deleted' => 1,);
        $this->db->where('id', $id);
        $this->db->update('career', $data);

        return $this->db->affected_rows();
    }

    public function edit_career($id, $data)
    {
        // edit time
        $now = date('Y-m-d H:i:s');
        $data['updated_at'] = $now;
        $this->db->where('id', $id);
        $this->db->update('career', $data);

        return $this->db->affected_rows();
    }

    public function get_service()
    {
        $data = array('is_deleted' => NULL,);
        $query = $this->db->get_where('service', $data);

        return $query->result();
    }

    public function create_service($data)
    {
        return ($this->db->insert('service', $data))  ?   $this->db->insert_id()  :   false;
    }

    public function delete_service($id)
    {
        // soft delete
        $data = array('is_deleted' => 1);
        $this->db->where('id', $id);
        $this->db->update('service', $data);

        return $this->db->affected_rows();
    }

    public function toggle_publish_service($id)
    {
        // get that id
        $this->db->select('is_published');
        $this->db->where('id', $id);
        $query = $this->db->get('service');
        foreach ($query->result() as $row) {
            $is_published = $row->is_published;
        }
        $is_published = ($is_published == '1') ? '0' : '1';

        $data = array('is_published' => $is_published);
        $this->db->where('id', $id);
        $this->db->update('service', $data);

        return $this->db->affected_rows();
    }

    public function edit_service($id, $data)
    {
        // edit time
        $now = date('Y-m-d H:i:s');
        $data['updated_at'] = $now;
        $this->db->where('id', $id);
        $this->db->update('service', $data);

        return $this->db->affected_rows();
    }
}
