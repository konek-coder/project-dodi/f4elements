<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->url = base_url();
		$this->public = base_url() . 'public';
		$this->load->model('m_dashboard');
		$this->res = base_url() . 'public/dist/js/';
	}

	public function index()
	{
		$this->load->view('templates/header');
		$this->load->view('dashboard');
		$this->load->view('templates/footer');
	}

	public function career()
	{
		$career = $this->m_dashboard->get_Career();
		$data['rows'] = $career;
		// load javascript
		$data['page_js'] = array(
			'<!--This page js -->',
			'<script src="' . $this->res . 'career.js"></script>'
		);
		$this->load->view('templates/header');
		$this->load->view('career', $data);
		$this->load->view('templates/footer');
	}

	public function createCareer()
	{
		$inputs = $this->input->post();
		$isInsert = $this->m_dashboard->create_career($inputs);
		if (!$isInsert) {
			echo 'An Error has occured. Please try again.';
		}else{
			redirect('dashboard/career');
		}
	}

	public function updateCareer($id)
	{
		$inputs = $this->input->post();
		$isProcessOK = $this->m_dashboard->edit_career($id, $inputs);
		if (!$isProcessOK) {
			echo 'An Error has occured. Please try again.';
		}else{
			redirect('dashboard/career');
		}
	}

	public function deleteCareer($id)
	{
		$isProcessOK = $this->m_dashboard->delete_career($id);
		if (!$isProcessOK) {
			echo 'An Error has occured. Please try again.';
		}else{
			redirect('dashboard/career');
		}
	}

	public function services()
	{
		$rows = $this->m_dashboard->get_service();
		$data['rows'] = $rows;
		// load javascript
		$data['page_js'] = array(
			'<!--This page js -->',
			'<script src="' . $this->res . 'service.js"></script>'
		);
		$this->load->view('templates/header');
		$this->load->view('services',$data);
		$this->load->view('templates/footer');
	}

	public function createService()
	{
		$inputs = $this->input->post();
		$isInsert = $this->m_dashboard->create_service($inputs);
		if (!$isInsert) {
			echo 'An Error has occured. Please try again.';
		}else{
			redirect('dashboard/services');
		}
	}

	public function updateService($id)
	{
		$inputs = $this->input->post();
		$isProcessOK = $this->m_dashboard->edit_service($id, $inputs);
		if (!$isProcessOK) {
			echo 'An Error has occured. Please try again.';
		}else{
			redirect('dashboard/services');
		}
	}

	public function deleteService($id)
	{
		$isProcessOK = $this->m_dashboard->delete_service($id);
		if (!$isProcessOK) {
			echo 'An Error has occured. Please try again.';
		}else{
			redirect('dashboard/services');
		}
	}

	public function togglePublishService($id)
	{
		$isProcessOK = $this->m_dashboard->toggle_publish_service($id);
		if (!$isProcessOK) {
			echo 'An Error has occured. Please try again.';
		}else{
			redirect('dashboard/services');
		}
	}
}
