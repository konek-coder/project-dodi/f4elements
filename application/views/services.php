<!--app-content open-->
<div class="app-content">
	<div class="side-app">
		<!-- PAGE-HEADER -->
		<div class="page-header">
			<div>
				<h1 class="page-title">Service</h1>
			</div>
			<div class="ml-auto pageheader-btn">
				<!-- <a href="#" class="btn btn-secondary btn-icon text-white btn-add-item" data-toggle="modal" data-target="#serviceModal">
					<span>
						<i class="fe fe-plus"></i>
					</span> Add Service
				</a> -->
			</div>
		</div>
		<!-- PAGE-HEADER END -->

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header bg-transparent border-0">
						<h3 class="card-title">Service List</h3>
					</div>
					<div class="">
						<div class="grid-margin">
							<div class="">
								<div class="table-responsive">
									<table class="table card-table table-vcenter mb-0  align-items-center mb-0">
										<thead class="thead-light">
											<tr>
												<th>No</th>
												<th>Title</th>
												<th>Popup Title</th>
												<th>Brand Name</th>
												<th>Overview</th>
												<th>Others</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											<?php
											if (is_array($rows) || is_object($rows)) {
												foreach ($rows as $key => $row) { ?>
													<tr data-id="<?= $row->id ?>" data-is-published="<?= $row->is_published ?>">
														<td class=""><?= $key + 1; ?></td>
														<td class="title"><?= $row->title; ?></td>
														<td class="popup_title"><?= $row->popup_title; ?></td>
														<td class="brand_name"><?= $row->brand_name; ?></td>
														<td class="overview"><?= $row->overview; ?></td>
														<td class="others"><?= $row->others; ?></td>
														<td>
															<button class="btn btn-secondary btn-edit-item mb-1" data-toggle="modal" data-target="#serviceModal">
																<span><i class="fe fe-edit"></i></span>
															</button>
															<?php if ($row->is_published == '0') { ?>
																<button class="btn btn-info btn-toggle-item mb-1" title='Publish this item'>
																	<span><i class='fe fe-eye'></i></span>
																</button>
																<?php } else { ?>
																	<button class="btn btn-danger btn-toggle-item mb-1" title='Unpublish this item'>
																		<span><i class='fe fe-eye-off'></i></span>
																	</button>
															<?php } ?>
															<!-- <button class="btn btn-danger btn-del-item">
																<span><i class="fe fe-trash-2"></i></span>
															</button> -->
														</td>
													</tr>
											<?php }
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- modals -->
<div class="modal" id="serviceModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
	<!-- data-keyboard="false" -->
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="example-Modal3">Add Service</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="post" action="<?= base_url('/dashboard/createCareer') ?>">
				<div class="modal-body">
					<div class="form-group">
						<label for="title" class="form-control-label">Service Title:</label>
						<input type="text" class="form-control" id="title" name="title">
					</div>
					<div class="form-group">
						<label for="popup_title" class="form-control-label">Popup Title:</label>
						<input type="text" class="form-control" id="popup_title" name="popup_title">
					</div>
					<div class="form-group">
						<label for="brand_name" class="form-control-label">Brand Name:</label>
						<input type="text" class="form-control" id="brand_name" name="brand_name">
					</div>
					<div class="form-group">
						<label for="overview" class="form-control-label">Overview:</label>
						<textarea class="form-control" id="overview" name="overview" rows="3"></textarea>
					</div>
					<div class="form-group">
						<label for="others" class="form-control-label">Others:</label>
						<textarea class="form-control" id="others" name="others" rows="3"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary" value="Submit">
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	const base_url = "<?= base_url() ?>";
</script>