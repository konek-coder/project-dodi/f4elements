<!--app-content open-->
<div class="app-content">
	<div class="side-app">
		<!-- PAGE-HEADER -->
		<div class="page-header">
			<div>
				<h1 class="page-title">Job</h1>
			</div>
			<div class="ml-auto pageheader-btn">
				<a href="#" class="btn btn-secondary btn-icon text-white btn-add-job" data-toggle="modal" data-target="#jobModal">
					<span>
						<i class="fe fe-plus"></i>
					</span> Add Job
				</a>
			</div>
		</div>
		<!-- PAGE-HEADER END -->

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header bg-transparent border-0">
						<h3 class="card-title">Job List</h3>
					</div>
					<div class="">
						<div class="grid-margin">
							<div class="">
								<div class="table-responsive">
									<table class="table card-table table-vcenter mb-0  align-items-center mb-0">
										<thead class="thead-light">
											<tr>
												<th>No</th>
												<th>Title</th>
												<th>Code</th>
												<th>Description</th>
												<th>Requirements</th>
												<th>Location</th>
												<th>Salary</th>
												<th>Summary</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											<!-- <tr>
												<td>1</td>
												<td class="text-sm font-weight-600">Internship</td>
												<td>
												</td>
												<td>please check pricing Info </td>
												<td class="text-nowrap">July 13, 2018</td>
												<td>
													<button class="btn btn-primary btn-edit-job" data-toggle="modal" data-target="#jobModal">
														<span><i class="fe fe-edit"></i></span>
														Edit
													</button>
													<button class="btn btn-primary btn-del-job">
														<span><i class="fe fe-trash-2"></i></span>
														Delete
													</button>
												</td>
											</tr> -->
											<?php
											if (is_array($rows) || is_object($rows)) {
												foreach ($rows as $key => $row) { ?>
													<tr data-id="<?= $row->id ?>">
														<td><?= $key + 1; ?></td>
														<td class="title"><?= $row->title; ?></td>
														<td class="code"><?= $row->job_code; ?></td>
														<td class="description"><?= $row->job_desc; ?></td>
														<td class="requirements"><?= $row->requirements; ?></td>
														<td class="location"><?= $row->job_location; ?></td>
														<td class="salary"><?= $row->salary; ?></td>
														<td class="summary"><?= $row->summary; ?></td>
														<td class="action">
															<button class="btn btn-secondary btn-edit-job mb-1" data-toggle="modal" data-target="#jobModal">
																<span><i class="fe fe-edit"></i></span>
															</button>
															<button class="btn btn-danger btn-del-job" data-id="<?= $row->id; ?>">
																<span><i class="fe fe-trash-2"></i></span>
															</button>
														</td>
													</tr>
											<?php }
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- modals -->
<div class="modal" id="jobModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
	<!-- data-keyboard="false" -->
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="example-Modal3">Add Job</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="post" action="<?= base_url('/dashboard/createCareer') ?>">
				<div class="modal-body">
					<div class="row">
						<div class="form-group col-sm-6">
							<label for="job-title" class="form-control-label">Job Title:</label>
							<input type="text" class="form-control" id="job-title" name="title">
						</div>
						<div class="form-group col-sm-6">
							<label for="job-code" class="form-control-label">Job Code:</label>
							<input type="text" class="form-control" id="job-code" name="job_code">
						</div>
					</div>
					<div class="form-group">
						<label for="job-description" class="form-control-label">Job Description:</label>
						<textarea class="form-control" id="job-description" rows="6" name="job_desc"></textarea>
					</div>
					<div class="form-group">
						<label for="job-requirements" class="form-control-label">Job Requirements:</label>
						<textarea class="form-control" id="job-requirements" name="requirements" rows="3"></textarea>
					</div>
					<div class="row">
						<div class="form-group col-sm-6">
							<label for="job-location" class="form-control-label">Job Location:</label>
							<input type="text" class="form-control" id="job-location" name="job_location">
						</div>
						<div class="form-group col-sm-6">
							<label for="job-salary" class="form-control-label">Job Salary:</label>
							<input type="text" class="form-control" id="job-salary" name="salary">
						</div>
					</div>
					<div class="form-group">
						<label for="job-summary" class="form-control-label">Job Summary:</label>
						<textarea class="form-control" id="job-summary" name="summary" rows="3"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary" value="Submit">
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	const base_url = "<?= base_url() ?>";
</script>