<!--app-content open-->
<div class="app-content">
    <div class="side-app">
        <!-- PAGE-HEADER -->
        <div class="page-header">
            <div>
                <h1 class="page-title">Overview</h1>
            </div>
            <div class="ml-auto pageheader-btn">
                <!-- <a href="#" class="btn btn-secondary btn-icon text-white btn-add-item" data-toggle="modal" data-target="#serviceModal">
					<span>
						<i class="fe fe-plus"></i>
					</span> Add Service
				</a> -->
            </div>
        </div>
        <!-- PAGE-HEADER END -->
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="card-order">
                            <h2 class="text-right "><i class="mdi mdi-account-multiple icon-size float-left text-primary text-primary-shadow"></i><span>Career</span></h2>
                            <a class="mb-0" href="<?= base_url('dashboard/career')?>">See More</span></a>
                        </div>
                    </div>
                </div>
            </div><!-- COL END -->
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card ">
                    <div class="card-body">
                        <div class="card-widget">
                            <h2 class="text-right"><i class="mdi mdi-cube icon-size float-left text-success text-success-shadow"></i><span>Services</span></h2>
                            <a class="mb-0" href="<?= base_url('dashboard/services')?>">See More</span></a>
                        </div>
                    </div>
                </div>
            </div><!-- COL END -->
        </div>
       

    </div>
</div>