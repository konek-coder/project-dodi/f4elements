<!doctype html>
<html lang="en" dir="ltr">

<head>
	<!-- META DATA -->
	<meta charset="UTF-8">
	<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Yoha –  HTML5 Bootstrap Admin Template">
	<meta name="author" content="Spruko Technologies Private Limited">
	<meta name="keywords" content="admin dashboard html template, admin dashboard template bootstrap 4, analytics dashboard templates, best admin template bootstrap 4, best bootstrap admin template, bootstrap 4 template admin, bootstrap admin template premium, bootstrap admin ui, bootstrap basic admin template, cool admin template, dark admin dashboard, dark admin template, dark dashboard template, dashboard template bootstrap 4, ecommerce dashboard template, html5 admin template, light bootstrap dashboard, sales dashboard template, simple dashboard bootstrap 4, template bootstrap 4 admin">

	<!-- FAVICON -->
	<!-- <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->public?>/assets/images/brand/favicon.ico" /> -->
	<link rel="icon" type="image/png" href="<?php echo $this->public?>/landing/assets/image/logo/F4ELEMENTS.png" />

	<!-- TITLE -->
	<title>Demo Satukelas by Koneksi Group</title>

	<!-- BOOTSTRAP CSS -->
	<link href="<?php echo $this->public?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

	<!-- STYLE CSS -->
	<link href="<?php echo $this->public?>/assets/css/style.css" rel="stylesheet" />
	<link href="<?php echo $this->public?>/assets/css/skin-modes.css" rel="stylesheet" />
	<link href="<?php echo $this->public?>/assets/css/dark-style.css" rel="stylesheet" />

	<!-- SIDE-MENU CSS -->
	<link href="<?php echo $this->public?>/assets/css/sidemenu.css" rel="stylesheet">

	<!--PERFECT SCROLL CSS-->
	<link href="<?php echo $this->public?>/assets/plugins/p-scroll/perfect-scrollbar.css" rel="stylesheet" />

	<!-- CUSTOM SCROLL BAR CSS-->
	<link href="<?php echo $this->public?>/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

	<!--- FONT-ICONS CSS -->
	<link href="<?php echo $this->public?>/assets/css/icons.css" rel="stylesheet" />

	<!-- SIDEBAR CSS -->
	<link href="<?php echo $this->public?>/assets/plugins/sidebar/sidebar.css" rel="stylesheet">

	<!-- COLOR SKIN CSS -->
	<link id="theme" rel="stylesheet" type="text/css" media="all" href="<?php echo $this->public?>/assets/colors/color1.css" />

	<!-- Override styles -->
	<style>
		table ul{
			list-style-type:initial;
			padding-inline-start: 16px;
		}
	</style>
</head>

<body class="app sidebar-mini">

	<!-- GLOBAL-LOADER -->
	<div id="global-loader">
		<img src="<?php echo $this->public?>/assets/images/loader.svg" class="loader-img" alt="Loader">
	</div>
	<!-- /GLOBAL-LOADER -->

	<!-- PAGE -->
	<div class="page">
		<div class="page-main">
			<!--APP-SIDEBAR-->
			<aside class="app-sidebar">
				<div class="side-header">
					<a class="header-brand1" href="<?= base_url('dashboard') ?>">
						<img src="<?php echo $this->public?>/assets/images/brand/logo.png" class="header-brand-img desktop-logo" alt="logo">
						<img src="<?php echo $this->public?>/assets/images/brand/logo-1.png" class="header-brand-img toggle-logo" alt="logo">
						<img src="<?php echo $this->public?>/landing/assets/image/logo/F4ELEMENTS.png" class="header-brand-img light-logo" alt="logo">
						<img src="<?php echo $this->public?>/landing/assets/image/logo/logo-hr.png" class="header-brand-img light-logo1" alt="logo" width="130px" height="16px" style="width:unset;height:unset;">
					</a><!-- LOGO -->
				</div>
				<!--APP-SIDEBAR-->
				<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
				<aside class="app-sidebar">
					<div class="side-header">
						<a class="header-brand1" href="<?= base_url('dashboard') ?>">
							<img src="<?php echo $this->public?>/assets/images/brand/logo.png" class="header-brand-img desktop-logo" alt="logo">
							<img src="<?php echo $this->public?>/assets/images/brand/logo-1.png" class="header-brand-img toggle-logo" alt="logo">
							<img src="<?php echo $this->public?>/landing/assets/image/logo/F4ELEMENTS.png" class="header-brand-img light-logo" alt="logo">
							<img src="<?php echo $this->public?>/landing/assets/image/logo/logo-hr.png" class="header-brand-img light-logo1" alt="logo" width="130px" height="16px" style="width:unset;height:unset;">
						</a>
						<!-- LOGO -->
					</div>
					<ul class="side-menu">
						<li class="slide">
							<a class="side-menu__item" href="<?=base_url()?>dashboard/career">
								<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24" class="side-menu__icon">
									<path d="M0 0h24v24H0V0z" fill="none" />
									<path d="M6.26 9L12 13.47 17.74 9 12 4.53z" opacity=".3" />
									<path d="M19.37 12.8l-7.38 5.74-7.37-5.73L3 14.07l9 7 9-7zM12 2L3 9l1.63 1.27L12 16l7.36-5.73L21 9l-9-7zm0 11.47L6.26 9 12 4.53 17.74 9 12 13.47z" />
								</svg>
								<span class="side-menu__label">Career</span>
							</a>
						</li>
						<li class="slide">
							<a class="side-menu__item" href="<?=base_url()?>dashboard/services">
								<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24" class="side-menu__icon">
									<path d="M0 0h24v24H0V0z" fill="none" />
									<path d="M6.26 9L12 13.47 17.74 9 12 4.53z" opacity=".3" />
									<path d="M19.37 12.8l-7.38 5.74-7.37-5.73L3 14.07l9 7 9-7zM12 2L3 9l1.63 1.27L12 16l7.36-5.73L21 9l-9-7zm0 11.47L6.26 9 12 4.53 17.74 9 12 13.47z" />
								</svg>
								<span class="side-menu__label">Services</span>
							</a>
						</li>
					</ul>
				</aside>
				<!--/APP-SIDEBAR-->
				<!-- <ul>
					<li class="slide">
						<a class="side-menu__item" href="sertifikasi.html">
							<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24" class="side-menu__icon">
								<path d="M0 0h24v24H0V0z" fill="none" />
								<path d="M6.26 9L12 13.47 17.74 9 12 4.53z" opacity=".3" />
								<path d="M19.37 12.8l-7.38 5.74-7.37-5.73L3 14.07l9 7 9-7zM12 2L3 9l1.63 1.27L12 16l7.36-5.73L21 9l-9-7zm0 11.47L6.26 9 12 4.53 17.74 9 12 13.47z" />
							</svg>
							<span class="side-menu__label">Career</span>
						</a>
					</li>
				</ul> -->
			</aside>
			<!--/APP-SIDEBAR-->

			<!-- App-Header -->
			<div class="app-header header">
				<div class="container-fluid">
					<div class="d-flex">
						<a class="header-brand d-md-none" href="<?= base_url('dashboard') ?>">
							<img src="<?php echo $this->public?>/landing/assets/image/logo/logo-hr.png" class="header-brand-img mobile-icon" alt="logo" style="width:unset;height:unset; position: relative;top: 50%;transform: translateY(-50%);margin-top:0;">
							<img src="<?php echo $this->public?>/assets/images/brand/logo.png" class="header-brand-img desktop-logo mobile-logo" alt="logo">
						</a>
						<a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#">
							<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
								<path d="M0 0h24v24H0V0z" fill="none" />
								<path d="M21 11.01L3 11v2h18zM3 16h12v2H3zM21 6H3v2.01L21 8z" />
							</svg>
						</a><!-- sidebar-toggle-->
						<div class="d-flex ml-auto header-right-icons header-search-icon">
							<div class="dropdown profile-1">
								<a href="#" data-toggle="dropdown" class="nav-link pl-2 pr-2  leading-none d-flex">
									<span>
										<img src="<?php echo $this->public?>/assets/images/users/10.jpg" alt="profile-user" class="avatar  mr-xl-3 profile-user brround cover-image">
									</span>
									<div class="text-center mt-1 d-none d-xl-block">
										<h6 class="text-dark mb-0 fs-13 font-weight-semibold">Admin</h6>
									</div>
								</a>
								<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">

									<a class="dropdown-item" href="<?= base_url('/login')?>">
										<i class="dropdown-icon mdi  mdi-logout-variant"></i> Sign out
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /App-Header -->