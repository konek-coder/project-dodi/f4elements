// add item
$(document).on('click', '.btn-add-item',  (e) => {
    $('#serviceModal .modal-title').html('Add Service')

    const $serviceModal = $('#serviceModal');
    const $title = $serviceModal.find('#title')
    const $popup_title = $serviceModal.find('#popup_title')
    const $brand_name = $serviceModal.find('#brand_name')
    const $overview = $serviceModal.find('#overview')
    const $others = $serviceModal.find('#others')

    const tmp = 'Test'
    $title.val(tmp)
    $popup_title.val(tmp)
    $brand_name.val(tmp)
    $overview.val(tmp)
    $others.val(tmp)

    const url = `${base_url}dashboard/createService/`;
    $('#serviceModal form').attr('action', url)
})

// edit item
$(document).on('click', '.btn-edit-item', (e) => {
    $('#serviceModal .modal-title').html('Edit Service');
    
    const tr = $(e.target).closest('tr');
    const title = tr.find('.title').text()
    const popup_title = tr.find('.popup_title').text()
    const brand_name = tr.find('.brand_name').text()
    const overview = tr.find('.overview').text()
    const others = tr.find('.others').text()

    const $serviceModal = $('#serviceModal');
    const $title = $serviceModal.find('#title')
    const $popup_title = $serviceModal.find('#popup_title')
    const $brand_name = $serviceModal.find('#brand_name')
    const $overview = $serviceModal.find('#overview')
    const $others = $serviceModal.find('#others')

    $title.val(title)
    $popup_title.val(popup_title)
    $brand_name.val(brand_name)
    $overview.val(overview)
    $others.val(others)

    const id = $(e.target).closest('tr').data('id');
    const url = `${base_url}dashboard/updateService/${id}`;
    $('#serviceModal form').attr('action', url)
})

// delete item
$(document).on('click', '.btn-del-item', (e) => {
    const id = $(e.target).closest('tr').data('id');
    const url = `${base_url}dashboard/deleteService/${id}`;
    Swal.fire({
        title: "Delete",
        text: "Are you sure want to delete?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = url;
        }
    })
})

// delete item
$(document).on('click', '.btn-toggle-item', (e) => {
    const id = $(e.target).closest('tr').data('id');
    const url = `${base_url}dashboard/togglePublishService/${id}`;
    const is_published = $(e.target).closest('tr').data('is-published');
    const title = (is_published == '1') ? 'Unpublish' : 'Publish' ;
    Swal.fire({
        title: title,
        text: `Are you sure want to ${(is_published == '1') ? 'unpublish' : 'publish'} this item?`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = url;
        }
    })
})