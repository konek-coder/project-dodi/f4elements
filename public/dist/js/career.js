// edit job
$(document).on('click', '.btn-edit-job', (e) => {
    $('#jobModal .modal-title').html('Edit Job');
    
    const tr = $(e.target).closest('tr');
    const title = tr.find('.title').text()
    const code = tr.find('.code').text()
    const description = tr.find('.description').text()
    const requirements = tr.find('.requirements').text()
    const location = tr.find('.location').text()
    const salary = tr.find('.salary').text()
    const summary = tr.find('.summary').text()

    const $jobModal = $('#jobModal');
    const modalTitle = $jobModal.find('#job-title')
    const modalCode = $jobModal.find('#job-code')
    const modalDesc = $jobModal.find('#job-description')
    const modalReq = $jobModal.find('#job-requirements')
    const modalLoc = $jobModal.find('#job-location')
    const modalSalary = $jobModal.find('#job-salary')
    const modalSummary = $jobModal.find('#job-summary')

    modalTitle.val(title)
    modalCode.val(code)
    modalDesc.val(description)
    modalReq.val(requirements)
    modalLoc.val(location)
    modalSalary.val(salary)
    modalSummary.val(summary)

    const id = $(e.target).closest('tr').data('id');
    const url = `${base_url}dashboard/updateCareer/${id}`;
    $('#jobModal form').attr('action', url)
})

// add job
$(document).on('click', '.btn-add-job',  (e) => {
    $('#jobModal .modal-title').html('Add Job')

    const $jobModal = $('#jobModal');
    const modalTitle = $jobModal.find('#job-title')
    const modalCode = $jobModal.find('#job-code')
    const modalDesc = $jobModal.find('#job-description')
    const modalReq = $jobModal.find('#job-requirements')
    const modalLoc = $jobModal.find('#job-location')
    const modalSalary = $jobModal.find('#job-salary')
    const modalSummary = $jobModal.find('#job-summary')

    const tmp = 'Test'
    modalTitle.val(tmp)
    modalCode.val(tmp)
    modalDesc.val(tmp)
    modalReq.val(tmp)
    modalLoc.val(tmp)
    modalSalary.val(tmp)
    modalSummary.val(tmp)

    const url = `${base_url}dashboard/createCareer`;
    $('#jobModal form').attr('action', url)
})

// delete job
$(document).on('click', '.btn-del-job', (e) => {
    const id = $(e.target).closest('tr').data('id');
    const url = `${base_url}dashboard/deleteCareer/${id}`;
    Swal.fire({
        title: "Delete",
        text: "Are you sure want to delete this item?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = url;
        }
    })
})